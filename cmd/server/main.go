package main

import (
	"log/slog"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func handler(w http.ResponseWriter, r *http.Request) {
	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {

		slog.Info(err.Error())
	}
	defer socket.Close()

	i := 0

	for {
		i++

		msg := "New message (#" + strconv.Itoa(i) + ")"
		slog.Info(msg)
		socket.WriteMessage(websocket.TextMessage, []byte(msg))
		time.Sleep(5 * time.Second)
	}
}

func main() {

	http.HandleFunc("/ws", handler)
	http.ListenAndServe(":8050", nil)
	
}
